# What it is?
RandomWalk it the sandbox-app with simple GUI for [random walks](https://en.wikipedia.org/wiki/Random_walk)rendering. There are different options available: the number of steps for each walk and the total number of walks to draw, size of canvas, color scheme, prng algorithm etc.

<img src="graphics/screenshot_1.png">

## Build
Build tested only with Qt 5.7, GCC 6.3.0. Open command line, change to the project directory (which conains 'src' directory) and run:

 ```
 mkdir build
 cd build
 qmake ../src
 make
 ```

## Samples
<img src="graphics/1.png" width="600" height="400">
<img src="graphics/2.png" width="600" height="400">

