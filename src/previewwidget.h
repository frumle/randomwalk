#ifndef PREVIEWWIDGET_H
#define PREVIEWWIDGET_H

#include <QWidget>
#include "surface.h"

class PreviewWidget : public QWidget
{
    Q_OBJECT
public:
    explicit PreviewWidget(QWidget *parent = 0);
    void paintEvent(QPaintEvent *pe);
    void setPreviewSurface(const Surface &surface);
    void mouseReleaseEvent(QMouseEvent *me);

signals:
    void rightClicked(QPoint pos);

private:
    const Surface *mPreviewSurface;
};

#endif // PREVIEWWIDGET_H
