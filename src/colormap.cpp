#include "colormap.h"

#include <QDebug>
#include "assert.h"
#include "math.h"


const ColorMap ColorMap::Grayscale = ColorMap(QList<QRgb>{0xffffffff,
                                                          0xff000000});
const ColorMap ColorMap::Spectral = ColorMap(QList<QRgb>{0xff9e0142,
                                                         0xffd53e4f,
                                                         0xfff46d43,
                                                         0xfffdae61,
                                                         0xfffee08b,
                                                         0xffe6f598,
                                                         0xffabdda4,
                                                         0xff66c2a5,
                                                         0xff3288bd,
                                                         0xff5e4fa2});
const ColorMap ColorMap::Strange = ColorMap(QList<QRgb>{0x669e0142,
                                                          0x669e0142});

ColorMap::ColorMap()
{
    mColors.append(0xffffffff);
    mColors.append(0xff000000);
}

ColorMap::ColorMap(const QList<QRgb> &colors):
    mColors(colors)
{
}

QRgb ColorMap::getColor(const double &t) const
{
    const int colorsCount = mColors.size();
    const double step = 1.0/(colorsCount - 1);
    double t_local = (t > 0.0 ? (t < 1.0 ? t : 1.0) : 0.0)/step;
    int index = (int)(t_local);
    t_local -= index;

    if(index == colorsCount - 1){
        index--;
        t_local = 1.0;
    }

    const QRgb startColor = mColors.at(index);
    const QRgb endColor = mColors.at(index + 1);

    int a = round(qAlpha(startColor) + (qAlpha(endColor) - qAlpha(startColor))*t_local);
    int r = round(qRed(startColor) + (qRed(endColor) - qRed(startColor))*t_local);
    int g = round(qGreen(startColor) + (qGreen(endColor) - qGreen(startColor))*t_local);
    int b = round(qBlue(startColor) + (qBlue(endColor) - qBlue(startColor))*t_local);

    return qRgba(r, g, b, a);
}
