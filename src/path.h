#ifndef PATH_H
#define PATH_H

#include <vector>

class Path
{
public:
   struct Point{
       int x;
       int y;
   };

   /*  Constructors  */
   Path();
   Path(int maxLength);

   /* Resize points container. */
   void resize(unsigned int size);

   /* Resets path and sets 0-point to start point - (0, 0) by default. */
   void reset();

   /* Resets path and sets start point to (x, y) */
   void reset(int x, int y);

   /* @return number of points in the path.*/
   int size() const{
       return mPointsCount;
   }


   /* @return the distance between start and last points. */
   double length();

   /* @return the distance between start and last points. */
   int manhattanLength();

   /* @return points of the path. */
   const std::vector<Point>& points() const{
       return mPoints;
   }

   /* If space left then set last point to 'p' and adds it to path (increments path size by 1).
    * @return true if moved or false if there is no space left.
    * */
   bool moveTo(const Point& p);

   /* If space left then moves last point by (dx, dy) and adds it to path (increments path size by 1).
    * @return true if moved or false if there is no space left.
    * */
   bool moveBy(const int &dx, const int &dy);

   /* If space left then moves last point in specified direction:
    * 0 - left
    * 1 - top
    * 2 - right
    * 3 - bottom
    * @return true if moved or false if there is no space left or direction is wrong.
    * */
   bool moveIn(const int& direction);



private:
   std::vector<Point> mPoints;
   unsigned int mPointsCount;
   Point mStartPoint;
   Point mLastPoint;
};

#endif // PATH_H
