#ifndef SURFACE_H
#define SURFACE_H

#include <QImage>
#include <QColor>
#include "path.h"
#include <QMutex>

class Surface
{
public:
      Surface();
      Surface(const int &w, const int &h);

      QSize getSize() const;
      void resize(int w, int h);

      const QImage& getImage() const;

      void lock() const;
      void unlock() const;

      void clear(const QRgb &color);
      void drawPath(const Path &path, const QRgb &color);

      bool save(const QString &file_name) const;



   private:
      mutable QMutex mSurfaceMutex;
      int mCellSize;
      QImage mSurfaceImage;

};

#endif // SURFACE_H
