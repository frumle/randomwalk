#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "surface.h"
#include "walkrunnable.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void saveSurfaceImage();


private:
    Ui::MainWindow *ui;
    QAction *mSaveAction;

    Surface *mSurface;
    WalkRunnable *mWalkTask;

    void setupMenus();
};

#endif // MAINWINDOW_H
