#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QColor>
#include <QThreadPool>
#include <QDebug>
#include <QFileDialog>

#include "surface.h"
#include "walkrunnable.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    mSurface(new Surface(1200, 800))
{
    ui->setupUi(this);
    ui->previewWidget->setPreviewSurface(*mSurface);
    setupMenus();

    connect(ui->startButton, &QPushButton::clicked, [this](){
        // Check surface size according to ui controls
        const QSize userSize = QSize(ui->widthSpinBox->value(), ui->heightSpinBox->value());
        const QSize surfaceSize = mSurface->getSize();
        if(userSize != surfaceSize)
            mSurface->resize(userSize.width(), userSize.height());


        // Configure and start random walk generation/rendering asyn. task:
        const int pathStepsCount = ui->stepsCountSpinBox->value();
        const int walksCount = ui->walksCountSpinBox->value();
        const QRgb surfaceBackgroundColor = 0xff212121;
        const ColorMap &colorMap = ColorMap::Spectral;

        mWalkTask = new WalkRunnable(*mSurface,
                                     pathStepsCount,
                                     walksCount,
                                     surfaceBackgroundColor,
                                     colorMap);

        // Disable "Start" button and "Save" action on task started.
        connect(mWalkTask, &WalkRunnable::started, this, [this](){
            ui->startButton->setEnabled(false);
            mSaveAction->setEnabled(false);
        });

        // Update progress bar and surface preview on task progress.
        connect(mWalkTask, &WalkRunnable::progressChanged, this, [this](int progress){
            ui->progressBar->setValue(progress);
            ui->previewWidget->update();
        });

        // Enable "Start" button and "Save" action on task finished.
        // Remove active task (mWalkTask = null).
        connect(mWalkTask, &WalkRunnable::finished, this, [this](){
            ui->startButton->setEnabled(true);
            mSaveAction->setEnabled(true);
            ui->previewWidget->update();
            mWalkTask = NULL;
        });

        // Enable "Start" button and "Save" action on task cancelled.
        // Remove active task (mWalkTask = null).
        connect(mWalkTask, &WalkRunnable::cancelled, this, [this](){
            ui->startButton->setEnabled(true);
            ui->previewWidget->update();
            mWalkTask = NULL;
        });

        // Cancell running random walk task on window destroyed.
        connect(this, &MainWindow::destroyed, mWalkTask, &WalkRunnable::cancel);

        // Start random walk task in default thread pool.
        QThreadPool::globalInstance()->start(mWalkTask, 0);
    });

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::saveSurfaceImage()
{
    if(mWalkTask == NULL){
        QString filename = QFileDialog::getSaveFileName(this,
                               "Save file",
                               "",
                               ".jpg (*.jpg)");
       if(!filename.endsWith(".jpg", Qt::CaseInsensitive)){

       }else{
           mSurface->save(filename);
       }
    }
}

void MainWindow::setupMenus()
{
    mSaveAction = new QAction("Save", ui->previewWidget);
    connect(mSaveAction, &QAction::triggered, this, &MainWindow::saveSurfaceImage);

    connect(ui->previewWidget, &PreviewWidget::rightClicked, this, [this](QPoint pos){
        QPoint global = ui->previewWidget->mapToGlobal(pos);
        QMenu menu(ui->previewWidget);
        menu.addAction(mSaveAction);
        menu.exec(global);
    });


}
