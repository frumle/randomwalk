#include "testwidget.h"
#include <QPainter>
#include <QMouseEvent>

HeyWidget::HeyWidget(QWidget *parent) : QWidget(parent),
    mColorMap(ColorMap::Spectral)
{
    setMouseTracking(true);
}

void HeyWidget::paintEvent(QPaintEvent *pe)
{
    QPainter p(this);
    const int h = height();
    QPen pen;
    pen.setWidth(1);
    for(int i = 0, n = width(); i < n; i++){
        pen.setColor(mColorMap.getColor((double)i/n));
        p.setPen(pen);
        p.drawLine(i, 0, i, h);
    }
//    p.setBrush(QBrush(mColor));
//    p.drawRect(rect());
}

void HeyWidget::mouseMoveEvent(QMouseEvent *me)
{
    double x = (double)me->x()/width();
    mColor = mColorMap.getColor(x);
    update();
}
