#include "walkrunnable.h"

#include <QThread>
#include <QColor>

#include "colormap.h"

WalkRunnable::WalkRunnable(Surface &surface,
                           const int &nSteps,
                           const int& nWalks,
                           const QRgb& surfaceColor,
                           const ColorMap& pathColorMap) :
    QObject(NULL),
    mSurface(surface),
    mStepsCount(nSteps),
    mWalksCount(nWalks),
    mSurfaceColor(surfaceColor),
    mColorMap(pathColorMap),
    mIsCancelled(false)
{
    setAutoDelete(true);
}

void WalkRunnable::run()
{
    emit started();

    // Create paths
    std::vector<Path> paths(mWalksCount, Path(mStepsCount));

    // Generate paths

    QSize surfaceSize = mSurface.getSize();

    int surface_center_x = surfaceSize.width()/2;
    int surface_center_y = surfaceSize.height()/2;
    srand (time(NULL));
    for(Path& path : paths){
        if(mIsCancelled){
            emit cancelled();
            return;
        }
        path.reset(surface_center_x, surface_center_y);
        while(path.moveIn(rand() % 4));
        //        path.reset(surface_center_x, surface_center_y);
        //        while(path.moveTo({rand() % surfaceSize.width(), rand() % surfaceSize.height()}));
    }

    // Setup draw palette

    // Draw paths
    double rWalksCount = 1.0/(mWalksCount > 1 ? mWalksCount - 1 : 1);
    mSurface.clear(mSurfaceColor);
    double progress;
    for(int i = 0; i < mWalksCount; i++){

        if(mIsCancelled){
            emit cancelled();
            return;
        }

        progress = i*rWalksCount;
        QRgb pathColor = mColorMap.getColor(progress);
        mSurface.drawPath(paths[i], pathColor);
        emit progressChanged(round(100*progress));
        QThread::msleep(13);
    }

    emit finished();
}

void WalkRunnable::cancel()
{
    mIsCancelled = true;
}
