#include "path.h"
#include <math.h>
#include <assert.h>

Path::Path() :
    mPoints(100)
{
    reset(0, 0);
}

Path::Path(int maxLength):
    mPoints(maxLength > 0 ? maxLength : 100)
{
    reset(0, 0);
}

void Path::resize(unsigned int size)
{
    assert(size > 0);
    this->mPoints.resize(size);
    mPointsCount = mPointsCount <= size ? mPointsCount : size;\
}

void Path::reset()
{
    mPointsCount = 1;
    mLastPoint = mStartPoint;
    mPoints[0] = mStartPoint;
}

void Path::reset(int x, int y)
{
    mStartPoint.x = x;
    mStartPoint.y = y;
    reset();
}

double Path::length()
{
    int dx = mLastPoint.x - mStartPoint.x;
    int dy = mLastPoint.y - mStartPoint.y;
    return sqrt(dx*dx + dx*dy);
}

int Path::manhattanLength()
{
    int dx = mLastPoint.x - mStartPoint.x;
    int dy = mLastPoint.y - mStartPoint.y;
    return abs(dx) + abs(dy);
}

bool Path::moveTo(const Path::Point &p)
{
    if(mPointsCount < mPoints.size()){
       mPoints[mPointsCount++] = p;
       return true;
    }else
        return false;
}

bool Path::moveBy(const int &dx, const int &dy)
{
    if(mPointsCount < mPoints.size()){
       mLastPoint.x += dx;
       mLastPoint.y += dy;
       mPoints[mPointsCount++] = mLastPoint;
       return true;
    }else
        return false;
}

bool Path::moveIn(const int &direction)
{
    if(mPointsCount < mPoints.size()){
       switch(direction){
           case 0: mLastPoint.x -= 1; break;
           case 1: mLastPoint.y -= 1; break;
           case 2: mLastPoint.x += 1; break;
           case 3: mLastPoint.y += 1; break;
           default: return false;
       }
       mPoints[mPointsCount++] = mLastPoint;
       return true;
    }else
       return false;
}
