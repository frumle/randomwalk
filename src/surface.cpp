#include "surface.h"

#include <QImageWriter>
#include <QPainter>
#include "assert.h"
#include <QDebug>

Surface::Surface():
    mCellSize(1),
    mSurfaceImage(QImage(1200, 800, QImage::Format_ARGB32))
{
    mSurfaceImage.fill(Qt::white);
}

Surface::Surface(const int &w, const int &h):
    mCellSize(1),
    mSurfaceImage(QImage(w, h, QImage::Format_ARGB32))
{
    mSurfaceImage.fill(Qt::white);
}

QSize Surface::getSize() const
{
    return mSurfaceImage.size();
}

void Surface::resize(int w, int h)
{
    mSurfaceImage = QImage(w, h, QImage::Format_ARGB32);
    clear(Qt::white);
}

bool Surface::save(const QString &file_name) const
{
    QList<QByteArray> supportedFormats = QImageWriter::supportedImageFormats();
    if(file_name.contains(".jpg") && supportedFormats.contains("jpg")){
        return mSurfaceImage.save(file_name, "jpg", 100);
    }else if(file_name.contains(".png") && supportedFormats.contains("png")){
        return mSurfaceImage.save(file_name, "png", 100);
    }
    return false;
}

const QImage &Surface::getImage() const
{
    return mSurfaceImage;
}

void Surface::lock() const
{
    mSurfaceMutex.lock();
}

void Surface::unlock() const
{
    mSurfaceMutex.unlock();
}

void Surface::clear(const QRgb &color)
{
    lock();
    mSurfaceImage.fill(color);
    unlock();
}

void Surface::drawPath(const Path &path, const QRgb &color)
{
    lock();
    const int cellSize = mCellSize;
    const int left = 0;
    const int top = 0;
    const int right = (mSurfaceImage.width() - 1);
    const int bottom = (mSurfaceImage.height() - 1);

    QPainter painter(&mSurfaceImage);
    painter.setBrush(QBrush(color));
    painter.setPen(QPen(color));

    for(Path::Point p : path.points()){
        const int x = p.x*cellSize;
        const int y = p.y*cellSize;
        if(x >= left && x <= right && y >= top && y <= bottom)
            painter.drawRect(x, y, cellSize, cellSize);
    }
    unlock();
}
