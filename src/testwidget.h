#ifndef TESTWIDGET_H
#define TESTWIDGET_H

#include <QWidget>

#include "colormap.h"

class HeyWidget : public QWidget
{
    Q_OBJECT
public:
    explicit HeyWidget(QWidget *parent = 0);
    void paintEvent(QPaintEvent *pe);
    void mouseMoveEvent(QMouseEvent *me);

signals:

public slots:

private:
    QRgb mColor;
    ColorMap mColorMap;
};

#endif // TESTWIDGET_H
