#ifndef COLORMAP_H
#define COLORMAP_H

#include <QList>
#include <QColor>

class ColorMap
{
public:
    static const ColorMap Grayscale;
    static const ColorMap Spectral;
    static const ColorMap Strange;

    ColorMap();
    ColorMap(const QList<QRgb> &colors);
    QRgb getColor(const double &t) const;

private:
    QList<QRgb> mColors;
};

#endif // COLORMAP_H
