#ifndef WALKRUNNABLE_H
#define WALKRUNNABLE_H

#include <QObject>
#include <QRunnable>

#include "surface.h"

#include <QColor>
#include "path.h"
#include "surface.h"
#include "random"
#include "vector"
#include <QDebug>
#include "colormap.h"
#include "unistd.h"

class WalkRunnable : public QObject, public QRunnable
{
    Q_OBJECT
public:
    explicit WalkRunnable(Surface &surface,
                          const int &nSteps,
                          const int& nWalks,
                          const QRgb& surfaceColor,
                          const ColorMap& pathColorMap);
    void run();

signals:
    void started();
    void progressChanged(int progress);
    void finished();
    void cancelled();

public slots:
    void cancel();


private:
    Surface &mSurface;
    const int mStepsCount;
    const int mWalksCount;
    const QRgb mSurfaceColor;
    const ColorMap mColorMap;
    volatile bool mIsCancelled;
};

#endif // WALKRUNNABLE_H
