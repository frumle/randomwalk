#include "previewwidget.h"
#include <QPainter>
#include <QDebug>
#include <QMouseEvent>
#include <QMenu>

PreviewWidget::PreviewWidget(QWidget *parent) : QWidget(parent),
    mPreviewSurface(NULL)
{
}

void PreviewWidget::paintEvent(QPaintEvent *pe)
{
    pe->accept();
    QPainter p(this);
    if(mPreviewSurface != NULL){

        mPreviewSurface->lock();
        const QImage &previewImage = mPreviewSurface->getImage();

        if(!previewImage.isNull()){

            QRect srcRect = previewImage.rect();
            float aspectRatio = (float)srcRect.width()/srcRect.height();
            int w, h;
            h = height();
            w = (int)(h*aspectRatio);
            if(w > width()){
                w = width();
                h = (int)(w/aspectRatio);
            }
            QRect dstRect(0, 0, w, h);
            dstRect.translate((width() - w)/2, (height() - h)/2);
            p.drawImage(dstRect, previewImage, srcRect,
                        Qt::ImageConversionFlag::NoFormatConversion);
        }

        mPreviewSurface->unlock();

    }else{
        p.setBrush(Qt::white);
        p.drawRect(rect());
    }
}

void PreviewWidget::setPreviewSurface(const Surface &surface)
{
    mPreviewSurface = &surface;
}

void PreviewWidget::mouseReleaseEvent(QMouseEvent *me)
{
    if(me->button() == Qt::RightButton){
        if(me->x() >= 0 && me->x() <= width() && me->y() >= 0 && me->y() <= height()){
            emit rightClicked(me->pos());
        }
    }
}
